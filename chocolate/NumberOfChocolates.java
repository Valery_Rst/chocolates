package net.chocolate;

import java.util.Scanner;

public class NumberOfChocolates {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество имеющихся у вас денег: ");
        int money = scanner.nextInt();

        System.out.println("Введите стоимость одной шоколадки: ");
        int priceForOneChocolate = scanner.nextInt();

        if (money < priceForOneChocolate) {
            System.out.println("Некорректные данные. Недостаточно средст для покупки одной шоколадки.");
            return;
        }

        System.out.println("Укажите количество оберток от шоколадки, требуемых для обмена на дополнительную шоколадку(кроме 1 и 0)");
        int wrapperOfTheChocolate = scanner.nextInt();

        if(wrapperOfTheChocolate <= 1) {
            System.out.println("Ошибка. Нельзя указывать значения 0 и 1(в этом случае обмен на новую шоколадку будет происходить бесконечно)");
            return;
        }

        int chocolates = money / priceForOneChocolate;
        int wrapperCount = chocolates;

        while (wrapperCount >= wrapperOfTheChocolate) {
            wrapperCount = wrapperCount - wrapperOfTheChocolate;
            chocolates++;
            wrapperCount++;
        }
        System.out.println("В общей сумме вы съели " + chocolates + " шоколадок");
    }

}
